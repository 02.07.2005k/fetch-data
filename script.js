// AJAX (Asynchronous JavaScript and XML) – це технологія, що дозволяє веб-сторінці оновлювати дані без перезавантаження. 
// Вона корисна при розробці JavaScript тим, що дозволяє динамічно взаємодіяти з сервером 
// (отримувати або надсилати дані), не порушуючи роботу користувача, що покращує зручність і швидкодію веб-додатків.

// Практичне завдання 
function getFilms() {
    fetch('https://ajax.test-danit.com/api/swapi/films')
        .then(response => response.json())
        .then(films => {
            displayFilms(films);
        })
        .catch(error => console.error('Error fetching films:', error));
}


function displayFilms(films) {
    const filmsContainer = document.getElementById('films');
    films.forEach(film => {
        const filmDiv = document.createElement('div');
        filmDiv.innerHTML = `
            <h2>Episode ${film.episodeId}: ${film.name}</h2>
            <p>${film.openingCrawl}</p>
            <ul id="characters-${film.id}">Loading characters...</ul>
        `;
        filmsContainer.appendChild(filmDiv);
        getCharacters(film.id, film.characters);
    });
}


function getCharacters(filmId, characters) {
    const charactersList = document.getElementById(`characters-${filmId}`);
    Promise.all(characters.map(url => fetch(url).then(response => response.json())))
        .then(characters => {
            charactersList.innerHTML = ''; // очищаємо список
            characters.forEach(character => {
                const li = document.createElement('li');
                li.textContent = character.name;
                charactersList.appendChild(li);
            });
        })
        .catch(error => console.error('Error fetching characters:', error));
}

getFilms();